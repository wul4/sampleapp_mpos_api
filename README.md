Sample App using PayThunder mPOS API for Android 
==========================================

[PayThunder](http://www.paythunder.com/) allows mobile payments & marketing as well as additional services combined with merchants applications, or working with the merchant POS. Besides the above, there is a demand from our customers and development community for a personalized version of a retailer app where they could request payments to consumers. Thus we have extended our PayThunder mPOS functionality with its API to enable the apps from our partners for this functionality. Thus these apps could enhance their existing functionality with PayThunder mPOS API. Currently the API supports payments but in future could be extended with additional PayThunder services.

For illustration purposes, this repository contains a sample project which integrates mPOS API for making PayThunder payments requests.

Integration instructions
------------------------

### Requirements

*   Android SDK version 15 or later.
*   PayThunder mPOS version 1.5.5 or later installed
*   Availability of ‘valid’ user credentials by the app developer for the merchant application that will invoke PayThunder mPOS API. For this    
    purpose contact to info[at]paythunder.com

### Setup


##### If you use gradle, then add the following dependency:

```
￼compile 'com.squareup.retrofit:retrofit:1.9.0'
￼compile 'com.squareup.okhttp:okhttp:2.4.0'

compile project(':PayThunder_mPOS')
```

##### If you use something other than gradle, then:

1. Edit AndroidManifest.xml. We're going to add a few additional items in here:

    ```xml
    <uses-sdk android:minSdkVersion="15" />
    ```

2. Also in your `<manifest>` element, make sure the following permissions and features are present:

    ```xml
    <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
    <uses-permission android:name="android.permission.ACCESS_WIFI_STATE" />
    ```

3. Within the `<application>` element, add activity entries:

    ```
    <!-- Activities responsible for gathering payment info -->
    <activity android:name="com.paythunder.mpos.api.PayThunderPaymentActivity" />
    ```

4. Within the `<application>` element, add API KEY entry:

    ```
    <meta-data 
      android:name="com.paythunder.API_KEY" 
      android:value="*** INSERT YOUR API KEY HERE (Provided by PayThunder)***"/>
    ```

##### Note: Before you build in release mode, make sure to adjust your proguard configuration by adding the following to `proguard.cnf`:

```
-keep class com.paythunder.**
-keepclassmembers class com.paythunder.** {
    *;
}
```

### Sample code  (See the Testsampleapp for an example)

Make your Activity implements IPayThunderInit interface:

```java
public class MainActivity extends AppCompatActivity implements IPayThunderInit
```

Initialize PayThunder by calling PayThunderAccess.init method on ‘onCreate’:

```java
@Override
protected void onCreate(Bundle savedInstanceState) {
  super.onCreate(savedInstanceState); [...]
  // Init PayThunder
  PayThunderAccess.init(this); 
}
```

Implements the two methods provided: initSuccess and initError.

```java
@Override
public void initSuccess() { 
  // init OK
}

@Override
public void initError(String error) { 
  // init error
  // do something with the error
}
```

First, we'll assume that you're going to launch the request from a button,
and that you've set the button's `onClick` handler in the layout XML via `android:onClick="requestPayThunderMPOS"`.
Then, add the method as:

```java
public void requestPayThunderMPOS(View v) {
  ￼Intent payThunderIntent = new Intent(this,
￼  PayThunderPaymentActivity.class);
￼  Bundle bundle = new Bundle();
￼  bundle.putString(Constants.REQUEST_AMOUNT, "15.00");}
￼  bundle.putString(Constants.REQUEST_CURRENCY, "EUR");
  ￼bundle.putString(Constants.REQUEST_CONCEPT, "THIS IS A PAYMENT FOR ILLUSTRATION PURPOSES");
￼
  // OPTIONAL PARAMETER
  ￼bundle.putString(Constants.REQUEST_PHONE, "666666666");
￼
  // OPTIONAL PARAMETER (DEFAULT VALUE IS "+34")
  bundle.putString(Constants.REQUEST_PHONE_CC, "+34");
￼
  // OPTIONAL PARAMETER
￼  bundle.putString(Constants.REQUEST_EMAIL, "test@email.com");}

  // OPTIONAL PARAMETER (false by default)
  bundle.putBoolean(Constants.REQUEST_BACKGROUND, true);

  // OPTIONAL PARAMETER
￼  bundle.putString(Constants.REQUEST_MERCHANT_ID, "t012016001");}
￼
  // OPTIONAL PARAMETER
￼  bundle.putString(Constants.REQUEST_USER_COM, "testCOMuser");￼  

  // PAYTHUNDER_REQUEST is arbitrary and is only used within this activity.
￼  payThunderIntent.putExtras(bundle);
￼  startActivityForResult(payThunderIntent, PAYTHUNDER_REQUEST);
}
```

Next, we'll override `onActivityResult()` to get the request result.

```java
@Override
￼protected void onActivityResult(int requestCode, int resultCode, Intent data) {
￼  super.onActivityResult(requestCode, resultCode, data);
￼  
  if (requestCode == PAYTHUNDER_REQUEST) {
      ￼switch (resultCode) {
             
              ￼case RESULT_OK:
                             if (data != null) {
                                                String codAutorizacion = extras.getString(DATA_CODAUTORIZACION);
                                                String respuesta = extras.getString(DATA_RESPUESTA);
                                                String telefono = extras.getString(DATA_TELEFONO);
                                                String importe = extras.getString(DATA_IMPORTE);
                                                String tipoMedioPago = extras.getString(DATA_TIPOMEDIOPAGO);
                                                String fecha = extras.getString(DATA_FECHA);
                                                String moneda = extras.getString(DATA_MONEDA);
                                                String peticion = extras.getString(DATA_PETICION);
                                                String confirmarPago = extras.getString(DATA_CONFIRMARPAGO);
                                                String operacionId = extras.getString(DATA_IDOPERACION);
                                                String merchantRequestID = extras.getString(DATA_MERCHANTREQUESTID);
                                                
                                                // do something with dataPaymentReply                                               
                                                }

                             ￼￼break;

              case RESULT_FIRST_USER: 
                             if (data != null) {
                                                error = data.getStringExtra(PayThunderPaymentActivity.EXTRA_PAYMENT_RESULT);
                                                error_code = data.getIntExtra(PayThunderPaymentActivity.EXTRA_PAYMENT_RESULT_CODE,-1);

                                                switch (error_code){
                                                        
                                                         case PayThunderPaymentActivity.ERROR_CODE_NO_MPOS: 
                                                                                                            // PayThunder mPOS App not installed.
                                                                                                            // do something 
                                                                                                            break;

                                                         case PayThunderPaymentActivity.ERROR_CODE_NO_INTERNET_CONNECTION: 
                                                                                                            // No Internet connection available.
                                                                                                            // do something 
                                                                                                            break;

                                                         case PayThunderPaymentActivity.ERROR_CODE_PAYTHUNDER_NO_INITIATED: 
                                                                                                            // PayThunder mPOS API has not been
                                                                                                            // initialized.
                                                                                                            // do something
                                                                                                            break;

                                                         case PayThunderPaymentActivity.ERROR_CODE_INVALID_SESSION: 
                                                                                                            // The session is expired. 
                                                                                                            // You must login.
                                                                                                            // do something
                                                                                                            break;

                                                         case PayThunderPaymentActivity.ERROR_CODE_INVALID_CREDENTIALS: 
                                                                                                            // The credentials provided 
                                                                                                            // are invalid.
                                                                                                            // do something 
                                                                                                            break;

                                                         case PayThunderPaymentActivity.ERROR_CODE_REQUIRED_PARAM_MISSING: 
                                                                                                            // A required parameter is missing.
                                                                                                            // do something
                                                                                                            break;

                                                         default:
                                                                  // Unexpected error.
                                                                  // do something 
                                                                  break;
                                                             }

                                                         // show error description
                                                         showErrorDialog(error);

                                                        }
                                                        break;

              case RESULT_CANCELED:
                    // do something with canceled operation
                    break;


              case RESULT_PAYMENT_DEFERRED:
                    // The payment Request was sent to background
                    // do something
                    break;

              default:

                    // error               
                    break;
            }
        }
```